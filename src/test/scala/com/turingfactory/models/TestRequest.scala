package com.turingfactory.models
import org.scalatest._
import scala.collection.mutable.Map
import com.turingfactory.exceptions.RequestException

class TestRequest extends FlatSpec with BeforeAndAfter  {

    "constructor" should "return valid object" in {
        //Arrange
        var inputPath: String = "inputPath"
        var outputPath: String = "outputPath"
        var inputFileFormat: String = "inputFileFormat"
        var outputFileFormat: String = "outputFileFormat"
        var password: String = "password"
        var salt: String = "salt"
        var encryptorType: String = "encryptorType"
        var columnsToEncrypt: String = "col1, col2"
        
        //Act
        val request: Request = new Request(inputPath, outputPath, inputFileFormat, outputFileFormat, password, salt, encryptorType, columnsToEncrypt)

        //Assert
        assert(request.isInstanceOf[Request])
    }

    "addError" should "add an error when receive valid empty or null inputPath" in {
        //Arrange
        val request: Request = new Request()
        val errorMessage = "cannot be null or empty"
        //Act
        request.addError("inputPath", errorMessage)

        //Assert
        assert(request.errorList.size == 1)
        assert(request.errorList("inputPath") == errorMessage)

    }

    it should "add an error when receive valid empty or null outputPath" in {
        //Arrange
        val request: Request = new Request()
        val errorMessage = "cannot be null or empty"
        //Act
        request.addError("outputPath", errorMessage)

        //Assert
        assert(request.errorList.size == 1)
        assert(request.errorList("outputPath") == errorMessage)

    }

    it should "add an error when receive valid empty or null inputFileFormat" in {
        //Arrange
        val request: Request = new Request()
        val errorMessage = "cannot be null or empty"
        //Act
        request.addError("inputFileFormat", errorMessage)

        //Assert
        assert(request.errorList.size == 1)
        assert(request.errorList("inputFileFormat") == errorMessage)

    }

    it should "add an error when receive valid empty or null outputFileFormat" in {
        //Arrange
        val request: Request = new Request()
        val errorMessage = "cannot be null or empty"
        //Act
        request.addError("outputFileFormat", errorMessage)

        //Assert
        assert(request.errorList.size == 1)
        assert(request.errorList("outputFileFormat") == errorMessage)

    }

    it should "add an error when receive valid empty or null password" in {
        //Arrange
        val request: Request = new Request()
        val errorMessage = "cannot be null or empty"
        //Act
        request.addError("password", errorMessage)

        //Assert
        assert(request.errorList.size == 1)
        assert(request.errorList("password") == errorMessage)

    }

    it should "add an error when receive valid empty or null salt" in {
        //Arrange
        val request: Request = new Request()
        val errorMessage = "cannot be null or empty"
        //Act
        request.addError("salt", errorMessage)

        //Assert
        assert(request.errorList.size == 1)
        assert(request.errorList("salt") == errorMessage)

    }

    it should "add an error when receive valid empty or null encryptorType" in {
        //Arrange
        val request: Request = new Request()
        val errorMessage = "cannot be null or empty"
        //Act
        request.addError("encryptorType", errorMessage)

        //Assert
        assert(request.errorList.size == 1)
        assert(request.errorList("encryptorType") == errorMessage)

    }

    it should "add an error when receive valid empty or null columnsToEncrypt" in {
        //Arrange
        val request: Request = new Request()
        val errorMessage = "cannot be null or empty"
        //Act
        request.addError("columnsToEncrypt", errorMessage)

        //Assert
        assert(request.errorList.size == 1)
        assert(request.errorList("columnsToEncrypt") == errorMessage)

    }

    it should "add all errors when receive invalid input to create object" in {
        //Arrange
        var inputPath: String = ""
        var outputPath: String = ""
        var inputFileFormat: String = ""
        var outputFileFormat: String = ""
        var password: String = ""
        var salt: String = ""
        var encryptorType: String = ""
        var columnsToEncrypt: String = ""

        //Act
        val request: Request = new Request(inputPath, outputPath, inputFileFormat, outputFileFormat, password, salt, encryptorType, columnsToEncrypt)
        val errorMessage = "cannot be null or empty"
        val errorMessage_correlator = "need almost one condition to run"


        //Assert
        assert(request.errorList.size == 8)
        assert(request.errorList("inputPath") == errorMessage)
        assert(request.errorList("outputPath") == errorMessage)
        assert(request.errorList("inputFileFormat") == errorMessage)
        assert(request.errorList("outputFileFormat") == errorMessage)
        assert(request.errorList("password") == errorMessage)
        assert(request.errorList("salt") == errorMessage)
        assert(request.errorList("encryptorType") == errorMessage)
        assert(request.errorList("columnsToEncrypt") == errorMessage)

    }



    "isValid" should "return true when errorlist is empty" in {
        //Arrange
        var inputPath: String = "inputPath"
        var outputPath: String = "outputPath"
        var inputFileFormat: String = "inputFileFormat"
        var outputFileFormat: String = "outputFileFormat"
        var password: String = "password"
        var salt: String = "salt"
        var encryptorType: String = "encryptorType"
        var columnsToEncrypt: String = "col1, col2"
        val request: Request = new Request(inputPath, outputPath, inputFileFormat, outputFileFormat, password, salt, encryptorType, columnsToEncrypt)
        val expected: Boolean = true


        //Act
        val actual = request.isValid

        //Assert
        assert(actual == expected)
    }

    it should "produce an RequestException when errorlist is not empty" in {
        //Arrange
        var inputPath: String = "inputPath"
        var outputPath: String = "outputPath"
        var inputFileFormat: String = "inputFileFormat"
        var outputFileFormat: String = "outputFileFormat"
        var password: String = "password"
        var salt: String = "salt"
        var encryptorType: String = ""
        var columnsToEncrypt: String = "col1, col2"
        val request: Request = new Request(inputPath, outputPath, inputFileFormat, outputFileFormat, password, salt, encryptorType, columnsToEncrypt)
        //val expected: Boolean = false


        //Act

        val actual =
            intercept[RequestException] {
                val result = request.isValid

            }
        val testMap = Map("encryptorType" -> "cannot be null or empty")
        val testClass = "class com.turingfactory.exceptions.RequestException"
        val expected = "this arguments cannot be empty or null:"
        // assert(actual.getMessage == expected)
        // val actual = request.isValid

        //Assert
        assert(actual.getMessage == expected.concat(testMap.toString))
        //assert(actual.getValue == testClass)
    }

}