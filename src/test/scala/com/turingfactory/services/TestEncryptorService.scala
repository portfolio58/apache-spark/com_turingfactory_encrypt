package com.turingfactory.services
import com.turingfactory.adapters.{SparkSessionFactory, FileFormatOperator}
import com.turingfactory.exceptions._
import com.turingfactory.models._
import com.turingfactory.ports.Encryptor
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types.StringType
import org.scalamock.scalatest.MockFactory
import org.scalatest._
import com.typesafe.scalalogging.Logger


class TestEncryptorService extends FlatSpec with MockFactory {
    val mylog = Logger("TestEncryptorService")

    "constructor" should "return valid object" in {
        //Arrange

        //Act
        val operator: FileFormatOperator = mock[FileFormatOperator]
        val encryptor: Encryptor =  mock[Encryptor]
        val request: Request = mock[Request]

        val service = new EncryptorService(operator, encryptor)
        //Assert
        assert(service.isInstanceOf[EncryptorService])
    }

    it should "raises an IllegalArgumentException when receives null operator" in {
        //Arrange
        val operator: FileFormatOperator = null
        val encryptor: Encryptor =  mock[Encryptor]
        val request: Request = mock[Request]

        //Assert
        assertThrows[IllegalArgumentException] {
            val service = new EncryptorService(operator, encryptor)
        }
    }

    it should "raises an IllegalArgumentException when receives null encryptor" in {
        //Arrange
        val operator: FileFormatOperator = mock[FileFormatOperator]
        val encryptor: Encryptor = null
        val request: Request = mock[Request]

        //Assert
        assertThrows[IllegalArgumentException] {
            val service = new EncryptorService(operator, encryptor)
        }
    }

    "invoke" should "return response success on valid adapters and request" in {
        //Arrange
        var inputPath: String = "inputPath"
        var outputPath: String = "outputPath"
        var inputFileFormat: String = "inputFileFormat"
        var outputFileFormat: String = "outputFileFormat"
        var password: String = "password"
        var salt: String = "salt"
        var encryptorType: String = "encryptorType"
        var columnsToEncrypt: String = "col1, col2"
        var header: Boolean = true
        var mode: String = "overwrite"
        val request: Request = new Request(inputPath, outputPath, inputFileFormat, outputFileFormat, password, salt, encryptorType, columnsToEncrypt)

        val mockRequest: Request = mock[Request]
        val encryptor: Encryptor =  mock[Encryptor]

        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val df = session.emptyDataFrame
        val operator = mock[FileFormatOperator]
        val conversionMap = request.columnsToEncrypt.map(a => a -> StringType).toMap
        val tColumns = conversionMap.map(x=> (col(x._1), x._2))
        (operator.getTable _) expects(request.inputPath, request.inputFileFormat, header) returning(df)
        (operator.castColumns _) expects(df, tColumns) returning(df)
        (operator.transformColumns _) expects(df, request.columnsToEncrypt, *) returning(df)
        (operator.saveTable _) expects(request.outputPath, df, mode)
        (operator.close _) expects()
        val service = new EncryptorService(operator, encryptor)

        //Act
        val response = service.invoke(request)

        //Assert
        assert(response.isInstanceOf[ResponseSuccess])
        //assert(response.getType() == ResponseType.RESPONSE_SUCCESS.toString())
    }

    it should "produce an responseFailure when receive invalid request" in {

        //Arrange
        var inputPath: String = ""
        var outputPath: String = "outputPath"
        var inputFileFormat: String = "inputFileFormat"
        var outputFileFormat: String = "outputFileFormat"
        var password: String = "password"
        var salt: String = "salt"
        var encryptorType: String = "encryptorType"
        var columnsToEncrypt: String = "col1, col2"
        var header: Boolean = true
        var mode: String = "mode"

        val request: Request = new Request(inputPath, outputPath, inputFileFormat, outputFileFormat, password, salt, encryptorType, columnsToEncrypt)

        val mockRequest: Request = mock[Request]
        val encryptor: Encryptor =  mock[Encryptor]

        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val df = session.emptyDataFrame
        val operator = mock[FileFormatOperator]
        val conversionMap = request.columnsToEncrypt.map(a => a -> StringType).toMap
        val tColumns = conversionMap.map(x=> (col(x._1), x._2))
        val service = new EncryptorService(operator, encryptor)

        //Act
        val response = service.invoke(request)

        //Assert
        assert("class com.turingfactory.exceptions.RequestException" == response.getValue)
    }

    it should "produce an responseFailure when operator exception" in {
        
        //Arrange
        var inputPath: String = "inputPath"
        var outputPath: String = "outputPath"
        var inputFileFormat: String = "inputFileFormat"
        var outputFileFormat: String = "outputFileFormat"
        var password: String = "password"
        var salt: String = "salt"
        var encryptorType: String = "encryptorType"
        var columnsToEncrypt: String = "col1, col2"
        var header: Boolean = true
        var mode: String = "mode"

        val request: Request = new Request(inputPath, outputPath, inputFileFormat, outputFileFormat, password, salt, encryptorType, columnsToEncrypt)

        val mockRequest: Request = mock[Request]
        val encryptor: Encryptor =  mock[Encryptor]

        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val df = session.emptyDataFrame
        val operator = mock[FileFormatOperator]
        val conversionMap = request.columnsToEncrypt.map(a => a -> StringType).toMap
        (operator.getTable _) expects(inputPath, inputFileFormat, header) throwing(new FileFormatException("Path cannot be empty"))
        val service = new EncryptorService(operator, encryptor)

        //Act
        val response = service.invoke(request)

        //Assert
        assert("class com.turingfactory.exceptions.FileFormatException" == response.getValue)
    }

    // it should "produce an responseFailure when encryptor exception" in {
        
    //     //Arrange
    //     var inputPath: String = ""
    //     var outputPath: String = "outputPath"
    //     var inputFileFormat: String = "inputFileFormat"
    //     var outputFileFormat: String = "outputFileFormat"
    //     var password: String = "password"
    //     var salt: String = "salt"
    //     var encryptorType: String = "encryptorType"
    //     var columnsToEncrypt: String = "col1, col2"

    //     val request: Request = new Request(inputPath, outputPath, inputFileFormat, outputFileFormat, password, salt, encryptorType, columnsToEncrypt)

    //     val mockRequest: Request = mock[Request]
    //     val encryptor: Encryptor =  mock[Encryptor]

    //     val sessionFactory = new SparkSessionFactory()
    //     val session = sessionFactory.makeLocal()
    //     val df = session.emptyDataFrame
    //     val operator = mock[FileFormatOperator]
    //     val conversionMap = request.columnsToEncrypt.map(a => a -> StringType).toMap
    //     (operator.getTable _) expects(inputPath, inputFileFormat, header) throwing(new FileFormatException("Path cannot be empty"))
    //     val service = new EncryptorService(operator, encryptor)

    //     //Act
    //     val response = service.invoke(request)

    //     //Assert
    //     assert("class com.turingfactory.exceptions.EncryptorException" == response.getValue)
    // }

}
