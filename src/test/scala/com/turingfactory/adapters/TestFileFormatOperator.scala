package com.turingfactory.adapters
import com.turingfactory.exceptions.FileFormatException
import com.turingfactory.ports._
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.scalatest._
import scala.collection.mutable.ListBuffer
import org.apache.log4j.{Level, Logger}
import com.typesafe.scalalogging.{Logger => scalaLogger}
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.sql.Row
import org.apache.spark.sql.types.{IntegerType, StringType, StructField, StructType}
import org.apache.spark.sql.functions.{col, lit}
import org.apache.spark.sql.types.{DataType, _}

class TestFileFormatOperator extends FlatSpec with BeforeAndAfter {

    val mylog = scalaLogger("TestFileFormatOperator")
    Logger.getLogger("org").setLevel(Level.OFF)
    Logger.getLogger("akka").setLevel(Level.OFF)
    
    private def getTable(path: String, spark: SparkSession): DataFrame ={
        if (spark == null){
            throw new FileFormatException("Session cannot be null")
        }
        else{
            if(path == null){
                throw new FileFormatException("Path cannot be null")
            }
            else{
                if(path.isEmpty){
                    throw new FileFormatException("Path cannot be empty")
                }
                else{
                    try{
                        var df = spark.read.format("avro").load(path)
                        return df
                    }
                    catch{
                        case x: Exception =>
                            {
                                throw new FileFormatException("error loading the file")
                            }
                    }
                }
            }
        }
    }

    private def validatePath(path: String, spark: SparkSession): Boolean ={
        if (spark == null){
            throw new FileFormatException("Session cannot be null")
        }
        else{
            if(path == null){
                throw new FileFormatException("Path cannot be null")
            }
            else{
                if(path.isEmpty){
                    throw new FileFormatException("Path cannot be empty")
                }
                else{
                    val sc = spark.sparkContext
                    val conf = sc.hadoopConfiguration
                    val p: Path  = new Path(path)
                    val fs: FileSystem = p.getFileSystem(conf)
                    try{
                        return fs.exists(p)
                    } 
                    catch{
                        case x: Exception =>
                            {
                                throw new FileFormatException("error loading the file")
                            }
                    }
                }
            }
        }
    }

    private def countRowsTable(df: DataFrame): String ={
        return df.count().toString
    }

    private def getSchema(table: DataFrame): Seq[String] = {
        return table.schema.map(x => x.name).toSeq
    }



    "existFiles" should "return true when receive valid path" in {
        
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val path=getClass.getResource("/userdata1.avro").getPath()
        //Act
        val result: Boolean = operator.existFiles(path)
        
        //Assert
        assert(result == true)
    }

    it should "produce an FileFormatException when receive invalid SparkSession" in {
        //Arrange
        val sparkObj: SparkSession = null
        val operator: FileFormatOperator = new FileFormatOperator(sparkObj)
        val path = getClass.getResource("/userdata1.avro").getPath()
        val actual =
        intercept[FileFormatException] {
            operator.existFiles(path)
        }
        val expected = "Session cannot be null"
        assert(actual.getMessage == expected)
    }

    it should "produce an FileFormatException when receive empty path" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val path = ""
        val actual =
        intercept[FileFormatException] {
            operator.existFiles(path)
        }
        val expected = "Path cannot be empty"
        assert(actual.getMessage == expected)
    }

    it should "produce an FileFormatException when receive null path" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val path = null
        val actual =
        intercept[FileFormatException] {
            operator.existFiles(path)
        }
        val expected = "Path cannot be null"
        assert(actual.getMessage == expected)
    }

    "getTable" should "return an Spark DataFrame when receive valid path" in {
        
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val path = getClass.getResource("/userdata1.avro").getPath()
        val fileFormat = "avro"
        //Act
        val result: DataFrame = operator.getTable(path, fileFormat)

        //Assert
        assert(result.getClass.getSimpleName == "Dataset")
    }

    it should "produce an FileFormatException when receive invalid SparkSession" in {
        //Arrange
        val path = getClass.getResource("/userdata1.avro").getPath()
        val session: SparkSession = null
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val fileFormat = "avro"
        val actual =
        intercept[FileFormatException] {
            operator.getTable(path, fileFormat)
        }
        val expected = "Session cannot be null"
        assert(actual.getMessage == expected)
    }

    it should "produce an FileFormatException when receive empty path" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val path = ""
        val fileFormat = "avro"
        val actual =
        intercept[FileFormatException] {
            operator.getTable(path, fileFormat)
        }
        val expected = "Path cannot be empty"
        assert(actual.getMessage == expected)
    }

    it should "produce an FileFormatException when receive null path" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val path = null
        val fileFormat = "avro"
        val actual =
        intercept[FileFormatException] {
            operator.getTable(path, fileFormat)
        }
        val expected = "Path cannot be null"
        assert(actual.getMessage == expected)
    }

    it should "produce an FileFormatException when receive empty fileFormat" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val path = getClass.getResource("/userdata1.avro").getPath()
        val fileFormat = ""
        val actual =
        intercept[FileFormatException] {
            operator.getTable(path, fileFormat)
        }
        val expected = "fileFormat cannot be empty"
        assert(actual.getMessage == expected)
    }

    it should "produce an FileFormatException when receive null fileFormat" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val path = getClass.getResource("/userdata1.avro").getPath()
        val fileFormat = null
        val actual =
        intercept[FileFormatException] {
            operator.getTable(path, fileFormat)
        }
        val expected = "fileFormat cannot be null"
        assert(actual.getMessage == expected)
    }

    it should "produce an FileFormatException when receive invalid fileFormat" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val path = getClass.getResource("/userdata1.avro").getPath()
        val fileFormat = "notImplemented"
        val actual =
        intercept[FileFormatException] {
            operator.getTable(path, fileFormat)
        }
        val expected = s"fileFormat: $fileFormat not implemented"
        assert(actual.getMessage == expected)
    }


    "saveTable" should "return true when receive valid path and DataFrame and mode" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val path = getClass.getResource("/").getPath().concat("output_table/")
        import session.implicits._
        val actual = Seq(
            (8, "bat"),
            (64, "mouse"),
            (-27, "horse")
            ).toDF("number", "word")

        //Act
        operator.saveTable(path, actual)
        val validateTableResult: Boolean = this.validatePath(path, session)
        val expected: DataFrame = this.getTable(path, session)

        //Assert
        assert(validateTableResult == true || this.countRowsTable(actual) == this.countRowsTable(expected)) 
    }


    it should "produce an FileFormatException when receive empty path" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        import session.implicits._
        val df = Seq(
            (8, "bat"),
            (64, "mouse"),
            (-27, "horse")
            ).toDF("number", "word")
        val path=""
        val actual =
        intercept[FileFormatException] {
            operator.saveTable(path, df)
        }
        val expected = "Path cannot be empty"
        assert(actual.getMessage == expected)
    }

    it should "produce an FileFormatException when receive null path" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        import session.implicits._
        val df = Seq(
            (8, "bat"),
            (64, "mouse"),
            (-27, "horse")
            ).toDF("number", "word")
        val path= null
        val actual =
        intercept[FileFormatException] {
            operator.saveTable(path, df)
        }
        val expected = "Path cannot be null"
        assert(actual.getMessage == expected)
    }

    it should "produce an FileFormatException when receive null mode" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val mode = null
        val path = getClass.getResource("/").getPath().concat("output_table/")
        import session.implicits._
        val df = Seq(
            (8, "bat"),
            (64, "mouse"),
            (-27, "horse")
            ).toDF("number", "word")
        val actual =
        intercept[FileFormatException] {
            operator.saveTable(path, df, mode)
        }
        val expected = "mode cannot be null"
        assert(actual.getMessage == expected)
    }

    it should "produce an FileFormatException when receive empty mode" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val mode = ""
        val path = getClass.getResource("/").getPath().concat("output_table/")
        import session.implicits._
        val df = Seq(
            (8, "bat"),
            (64, "mouse"),
            (-27, "horse")
            ).toDF("number", "word")
        val actual =
        intercept[FileFormatException] {
            operator.saveTable(path, df, mode)
        }
        val expected = "mode cannot be empty"
        assert(actual.getMessage == expected)
    }


    it should "produce an FileFormatException when receive invalid mode" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val mode = "notImplemented"
        val path = getClass.getResource("/").getPath().concat("output_table/")
        import session.implicits._
        val df = Seq(
            (8, "bat"),
            (64, "mouse"),
            (-27, "horse")
            ).toDF("number", "word")
        val actual =
        intercept[FileFormatException] {
            operator.saveTable(path, df, mode)
        }
        val expected = s"mode: $mode not implemented"
        assert(actual.getMessage == expected)
    }


    "savePartitionedTable" should "return true when receive valid path, DataFrame, partitionedBy, fileFormat, header and mode" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val path = getClass.getResource("/").getPath().concat("output_partitioned_table/")
        val partitionedBy ="year,month,day".split(",").toSeq
        val fileFormat = "avro"
        import session.implicits._
        val actual = Seq(
            (8, "bat", "2020-10-01", "2020", "10", "01"),
            (64, "mouse", "2020-10-02", "2020", "10", "02"),
            (-27, "horse", "2020-10-03", "2020", "10", "03")
            ).toDF("number", "word", "date", "year", "month", "day")


        //Act
        operator.savePartitionedTable(path, actual, partitionedBy, fileFormat)
        val validateTableResult: Boolean = this.validatePath(path, session)
        val expected: DataFrame = this.getTable(path, session)

        //Assert
        assert(validateTableResult == true || this.countRowsTable(actual) == this.countRowsTable(expected)) 
    }

    it should "produce an FileFormatException when receive null path" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val path = null
        val partitionedBy ="year,month,day".split(",").toSeq
        val fileFormat = "avro"
        import session.implicits._
        val df = Seq(
            (8, "bat", "2020-10-01", "2020", "10", "01"),
            (64, "mouse", "2020-10-02", "2020", "10", "02"),
            (-27, "horse", "2020-10-03", "2020", "10", "03")
            ).toDF("number", "word", "date", "year", "month", "day")

        val actual =
        intercept[FileFormatException] {
            operator.savePartitionedTable(path, df, partitionedBy, fileFormat)
        }
        val expected = "Path cannot be null"
        assert(actual.getMessage == expected)
    }

    it should "produce an FileFormatException when receive empty path" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val path = ""
        val partitionedBy ="year,month,day".split(",").toSeq
        val fileFormat = "avro"
        import session.implicits._
        val df = Seq(
            (8, "bat", "2020-10-01", "2020", "10", "01"),
            (64, "mouse", "2020-10-02", "2020", "10", "02"),
            (-27, "horse", "2020-10-03", "2020", "10", "03")
            ).toDF("number", "word", "date", "year", "month", "day")

        val actual =
        intercept[FileFormatException] {
            operator.savePartitionedTable(path, df, partitionedBy, fileFormat)
        }
        val expected = "Path cannot be empty"
        assert(actual.getMessage == expected)
    }

    it should "produce an FileFormatException when receive null partitionedBy" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val path = getClass.getResource("/").getPath().concat("output_partitioned_table/")
        val partitionedBy: Seq[String] = null
        val fileFormat = "avro"
        import session.implicits._
        val df = Seq(
            (8, "bat", "2020-10-01", "2020", "10", "01"),
            (64, "mouse", "2020-10-02", "2020", "10", "02"),
            (-27, "horse", "2020-10-03", "2020", "10", "03")
            ).toDF("number", "word", "date", "year", "month", "day")

        val actual =
        intercept[FileFormatException] {
            operator.savePartitionedTable(path, df, partitionedBy, fileFormat)
        }
        val expected = "partitionedBy cannot be null"
        assert(actual.getMessage == expected)
    }

    it should "produce an FileFormatException when receive empty partitionedBy" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val path = getClass.getResource("/").getPath().concat("output_partitioned_table/")
        val partitionedBy: Seq[String] = Seq()
        val fileFormat = "avro"
        import session.implicits._
        val df = Seq(
            (8, "bat", "2020-10-01", "2020", "10", "01"),
            (64, "mouse", "2020-10-02", "2020", "10", "02"),
            (-27, "horse", "2020-10-03", "2020", "10", "03")
            ).toDF("number", "word", "date", "year", "month", "day")

        val actual =
        intercept[FileFormatException] {
            operator.savePartitionedTable(path, df, partitionedBy, fileFormat)
        }
        val expected = "partitionedBy cannot be empty"
        assert(actual.getMessage == expected)
    }


    it should "produce an FileFormatException when receive null fileFormat" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val path = getClass.getResource("/").getPath().concat("output_partitioned_table/")
        val partitionedBy ="year,month,day".split(",").toSeq
        val fileFormat = null
        import session.implicits._
        val df = Seq(
            (8, "bat", "2020-10-01", "2020", "10", "01"),
            (64, "mouse", "2020-10-02", "2020", "10", "02"),
            (-27, "horse", "2020-10-03", "2020", "10", "03")
            ).toDF("number", "word", "date", "year", "month", "day")

        val actual =
        intercept[FileFormatException] {
            operator.savePartitionedTable(path, df, partitionedBy, fileFormat)
        }
        val expected = "fileFormat cannot be null"
        assert(actual.getMessage == expected)
    }

    it should "produce an FileFormatException when receive empty fileFormat" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val path = getClass.getResource("/").getPath().concat("output_partitioned_table/")
        val partitionedBy ="year,month,day".split(",").toSeq
        val fileFormat = ""
        import session.implicits._
        val df = Seq(
            (8, "bat", "2020-10-01", "2020", "10", "01"),
            (64, "mouse", "2020-10-02", "2020", "10", "02"),
            (-27, "horse", "2020-10-03", "2020", "10", "03")
            ).toDF("number", "word", "date", "year", "month", "day")

        val actual =
        intercept[FileFormatException] {
            operator.savePartitionedTable(path, df, partitionedBy, fileFormat)
        }
        val expected = "fileFormat cannot be empty"
        assert(actual.getMessage == expected)
    }



    it should "produce an FileFormatException when receive null mode" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val path = getClass.getResource("/").getPath().concat("output_partitioned_table/")
        val partitionedBy ="year,month,day".split(",").toSeq
        val fileFormat = "avro"
        val header = true
        val mode = null
        import session.implicits._
        val df = Seq(
            (8, "bat", "2020-10-01", "2020", "10", "01"),
            (64, "mouse", "2020-10-02", "2020", "10", "02"),
            (-27, "horse", "2020-10-03", "2020", "10", "03")
            ).toDF("number", "word", "date", "year", "month", "day")

        val actual =
        intercept[FileFormatException] {
            operator.savePartitionedTable(path, df, partitionedBy, fileFormat, header, mode)
        }
        val expected = "mode cannot be null"
        assert(actual.getMessage == expected)
    }

    it should "produce an FileFormatException when receive empty mode" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val path = getClass.getResource("/").getPath().concat("output_partitioned_table/")
        val partitionedBy ="year,month,day".split(",").toSeq
        val fileFormat = "avro"
        val header = true
        val mode = ""
        import session.implicits._
        val df = Seq(
            (8, "bat", "2020-10-01", "2020", "10", "01"),
            (64, "mouse", "2020-10-02", "2020", "10", "02"),
            (-27, "horse", "2020-10-03", "2020", "10", "03")
            ).toDF("number", "word", "date", "year", "month", "day")

        val actual =
        intercept[FileFormatException] {
            operator.savePartitionedTable(path, df, partitionedBy, fileFormat, header, mode)
        }
        val expected = "mode cannot be empty"
        assert(actual.getMessage == expected)
    }




    "queryTable" should "return a dataframe when receive valid alias and valid query and valid DataFrame" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val alias="animals"
        val query="SELECT COUNT(*) FROM animals"
        import session.implicits._
        val df = Seq(
            (8, "bat"),
            (64, "mouse"),
            (-27, "horse")
            ).toDF("number", "word")

        //Act
        val result: DataFrame = operator.queryTable(df, query, alias)
        val actual = result.collect().map(_(0)).toList(0).toString
        val expected = "3"

        //Assert
        assert(actual == expected)
    }

    it should "produce an FileFormatException when receive invalid SparkSession" in {
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        import session.implicits._
        val sessionn: SparkSession = null
        val operator: FileFormatOperator = new FileFormatOperator(sessionn)
        //Arrange
        val alias="animals"
        val query="SELECT COUNT(*) FROM animals"
        val df = Seq(
            (8, "bat"),
            (64, "mouse"),
            (-27, "horse")
            ).toDF("number", "word")
        val actual =
        intercept[FileFormatException] {
            val result: DataFrame = operator.queryTable(df, query, alias)
        }
        val expected = "Session cannot be null"
        assert(actual.getMessage == expected)
    }

    it should "produce an FileFormatException when receive null alias" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val alias= null 
        val query="SELECT COUNT(*) FROM animals"
        import session.implicits._
        val df = Seq(
            (8, "bat"),
            (64, "mouse"),
            (-27, "horse")
            ).toDF("number", "word")

        val actual =
        intercept[FileFormatException] {
            val result: DataFrame = operator.queryTable(df, query, alias)
        }
        val expected = "Alias cannot be null"
        assert(actual.getMessage == expected)
    }

    it should "produce an FileFormatException when receive empty alias" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val alias = ""
        val query = "SELECT COUNT(*) FROM animals"
        import session.implicits._
        val df = Seq(
            (8, "bat"),
            (64, "mouse"),
            (-27, "horse")
            ).toDF("number", "word")
        
        val actual =
        intercept[FileFormatException] {
            val result: DataFrame = operator.queryTable(df, query, alias)
        }

        val expected = "Alias cannot be empty"
        assert(actual.getMessage == expected)
    }

    it should "produce an FileFormatException when receive null query" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val alias = "animals" 
        val query = null
        import session.implicits._
        val df = Seq(
            (8, "bat"),
            (64, "mouse"),
            (-27, "horse")
            ).toDF("number", "word")
        
        val actual =
        intercept[FileFormatException] {
            val result: DataFrame = operator.queryTable(df, query, alias)
        }
        val expected = "Query cannot be null"
        assert(actual.getMessage == expected)
    }

    it should "produce an FileFormatException when receive empty query" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val alias= "animals" 
        val query=""
        import session.implicits._
        val df = Seq(
            (8, "bat"),
            (64, "mouse"),
            (-27, "horse")
            ).toDF("number", "word")
        
        val actual =
        intercept[FileFormatException] {
            val result: DataFrame = operator.queryTable(df, query, alias)
        }
        val expected = "Query cannot be empty"
        assert(actual.getMessage == expected)
    }

    "castColumns" should "return a dataframe when receive valid dataframe and valid columns" in {
        
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val path = getClass.getResource("/").getPath().concat("/userdata1.avro")
        val actualDF: DataFrame = this.getTable(path, session)
        val columns: Map[String, DataType] = Map[String, DataType](
            "registration_dttm" -> StringType,
            "id" -> StringType,
            "first_name" -> StringType,
            "last_name" -> StringType,
            "email" -> StringType,
            "gender" -> StringType,
            "ip_address" -> StringType,
            "cc" -> StringType,
            "country" -> StringType,
            "birthdate" -> StringType,
            "salary" -> StringType,
            "title" -> StringType,
            "comments" -> StringType
        )
        
        //Act
        val expectedDF: DataFrame = operator.castColumns(actualDF, columns.map(x=> (col(x._1), x._2)))
        val expectedColumns : collection.mutable.Map[String, DataType] = collection.mutable.Map[String, DataType]()
        for((key,value)<- columns){
            var expectedDataType: DataType = expectedDF.schema.filter(x => x.name == key)(0).dataType
            expectedColumns += (key -> expectedDataType)

        }

        //Assert
        assert(columns.equals(expectedColumns.toMap))
    }



    it should "produce an FileFormatException when receive empty columns" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val path = getClass.getResource("/").getPath().concat("/userdata1.avro")
        val df: DataFrame = this.getTable(path, session)
        
        
        val columns: Map[String, DataType] = Map[String, DataType]()
        val actual =
        intercept[FileFormatException] {
            operator.castColumns(df, columns.map(x=> (col(x._1), x._2)))
        }
        val expected = "Columns cannot be empty"
        assert(actual.getMessage == expected)
    }

    "findFirstValidPath" should "return valid path when receive valid Paths" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val validPath: String = getClass.getResource("/").getPath().concat("/")
        val invalidPath: String = getClass.getResource("/").getPath().concat("empty_table/")
        val paths: Seq[String] = Seq(
            invalidPath,
            validPath
        )
        //Act
        val actual: String = operator.findFirstValidPath(paths)
        val expected = validPath
        //Assert
        assert(actual == expected)
    }

    it should "return empty path when receive invalid Paths" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val validPath: String = getClass.getResource("/").getPath().concat("/")
        val invalidPath: String = getClass.getResource("/").getPath().concat("empty_table/")
        val paths: Seq[String] = Seq(
            invalidPath,
            invalidPath
        )
        //Act
        val actual: String = operator.findFirstValidPath(paths)
        val expected = ""
        //Assert
        assert(actual == expected)
    }

    it should "produce an FileFormatException when receive empty Paths" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val paths: Seq[String] = Seq()
        //Act
        val actual =
        intercept[FileFormatException] {
            operator.findFirstValidPath(paths)
        }
        val expected = "Paths cannot be empty"
        //Assert
        assert(actual.getMessage == expected)
    }

    "selectColumns" should "return a dataframe when receive valid dataframe and valid columns" in {
        
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val path=getClass.getResource("/").getPath().concat("/userdata1.avro")
        val actualDF: DataFrame = this.getTable(path, session)
        val columns: Map[String, DataType] = Map[String, DataType](
            "registration_dttm" -> StringType,
            "id" -> StringType,
            "first_name" -> StringType
        )
        
        //Act
        val columnsNames = columns.keys.toSeq
        val expectedDF: DataFrame = operator.selectColumns(actualDF, columnsNames)
        val expectedColumns = ListBuffer[String]()
        for(column<- columnsNames){
            var expectedName: String = expectedDF.schema.filter(x => x.name == column)(0).name
            expectedColumns += expectedName

        }


        //Assert
        assert(columnsNames.equals(expectedColumns.toList.toSeq))
    }

    it should "produce an FileFormatException when receive empty columns" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val path=getClass.getResource("/").getPath().concat("/userdata1.avro")
        val actualDF: DataFrame = this.getTable(path, session)
        
        val columns: Map[String, DataType] = Map[String, DataType]()
        val columnsNames = columns.keys.toSeq
        //Act
        val actual =
        intercept[FileFormatException] {
            operator.selectColumns(actualDF, columnsNames)
        }
        val expected = "Columns cannot be empty"
        //Assert
        assert(actual.getMessage == expected)
    }


     it should "produce an FileFormatException when receive bad columns names" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val path = getClass.getResource("/").getPath().concat("/userdata1.avro")
        val actualDF: DataFrame = this.getTable(path, session)
        val columns: Map[String, DataType] = Map[String, DataType](
            "wrongcolumna" -> StringType,
            "wrongcolumnb" -> StringType,
            "wrongcolumnc" -> StringType
        )
        val columnsNames = columns.keys.toSeq
        //Act
        val actual =
        intercept[FileFormatException] {
            operator.selectColumns(actualDF, columnsNames)
            }
        val expected = "error selecting the columns"
        //Assert
        assert(actual.getMessage == expected)
    }

    "transformColumn" should "return a dataframe when receive valid dataframe and valid column and valid function" in {
        
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val password: String = "t3stpwd_"
        val salt: String = "07c92cfa3744bcdb" // test value
        val encryptor: Encryptor = new SpringEncryptor(password, salt)
        import session.implicits._
        val df = Seq(
            (8, "bat"),
            (64, "mouse"),
            (-27, "horse")
            ).toDF("number", "word")
        val expectedDF = Seq(
            ("e85bacaf180802162c5c6d76c6705c99", "bat"),
            ("c3464a59fd19edb5456e4019b9f18063", "mouse"),
            ("54bf3f8294f89b55059d3ec5fd45c7e4", "horse")
            ).toDF("number", "word")
        val expectedSeq = expectedDF.select("number").map( x => x.getString(0)).collect.toSeq
        val column = "number"
        //Act
        val actualDF: DataFrame = operator.transformColumn(df, column, encryptor.encrypt(_))
        //val actualDF = tmpDF.select("number").withColumn("number", df("number").cast(StringType)).map( x => x.getString(0)).collect.toList
        val actualSeq = actualDF.select("number").map( x => x.getString(0)).collect.toSeq

        //Assert
        assert(expectedSeq.equals(actualSeq))
    }


    it should "return FileFormatException when receive null column" in {

        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val password: String = "t3stpwd_"
        val salt: String = "07c92cfa3744bcdb" // test value
        val encryptor: Encryptor = new SpringEncryptor(password, salt)
        import session.implicits._
        val df = Seq(
            (8, "bat"),
            (64, "mouse"),
            (-27, "horse")
            ).toDF("number", "word")
        val column = null
        val actual =
        intercept[FileFormatException] {
            val actualDF: DataFrame = operator.transformColumn(df, column, encryptor.encrypt(_))
            }
        val expected = "Column cannot be null"
        //Assert
        assert(actual.getMessage == expected)
    }

    it should "return FileFormatException when receive empty column" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val password: String = "t3stpwd_"
        val salt: String = "07c92cfa3744bcdb" // test value
        val encryptor: Encryptor = new SpringEncryptor(password, salt)
        import session.implicits._
        val df = Seq(
            (8, "bat"),
            (64, "mouse"),
            (-27, "horse")
            ).toDF("number", "word")
        val column = ""
        val actual =
        intercept[FileFormatException] {
            val actualDF: DataFrame = operator.transformColumn(df, column, encryptor.encrypt(_))
            }
        val expected = "Column cannot be empty"
        //Assert
        assert(actual.getMessage == expected)
    }

    "transformColumns" should "return a dataframe when receive valid dataframe and valid column and valid function" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val password: String = "t3stpwd_"
        val salt: String = "07c92cfa3744bcdb" // test value
        val encryptor: Encryptor = new SpringEncryptor(password, salt)
        import session.implicits._
        val df = Seq(
            (8, "bat"),
            (64, "mouse"),
            (-27, "horse")
            ).toDF("number", "word")
        val expectedDF = Seq(
            ("e85bacaf180802162c5c6d76c6705c99", "043f7c5c61e7b8d9361c7327d7b25391"),
            ("c3464a59fd19edb5456e4019b9f18063", "e50a40367c0981179e04b57e50f75ccc"),
            ("54bf3f8294f89b55059d3ec5fd45c7e4", "e6de417cb1423dc07600b594d8bcbbae")
            ).toDF("number", "word")
        val expectedNumber = expectedDF.select("number").map( x => x.getString(0)).collect.toSeq
        val expectedWord = expectedDF.select("word").map( x => x.getString(0)).collect.toSeq
        val columns: Map[String, DataType] = Map[String, DataType](
            "number" -> StringType,
            "word" -> StringType
        )
        val columnSeq =  columns.keys.toSeq
        //Act
        
        val actualDF: DataFrame = operator.transformColumns(df, columnSeq, encryptor.encrypt(_))
        //val actualDF = tmpDF.select("number").withColumn("number", df("number").cast(StringType)).map( x => x.getString(0)).collect.toList
        val actualNumber = actualDF.select("number").map( x => x.getString(0)).collect.toSeq
        val actualWord = actualDF.select("word").map( x => x.getString(0)).collect.toSeq

        //Assert
        assert(expectedNumber.equals(actualNumber))
        assert(expectedWord.equals(actualWord))
    }



    it should "return FileFormatException when receive empty columns" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val password: String = "t3stpwd_"
        val salt: String = "07c92cfa3744bcdb" // test value
        val encryptor: Encryptor = new SpringEncryptor(password, salt)
        import session.implicits._
        val df = Seq(
            (8, "bat"),
            (64, "mouse"),
            (-27, "horse")
            ).toDF("number", "word")
        
        
        val columns: Map[String, DataType] = Map[String, DataType]()
        val columnSeq =  columns.keys.toSeq
        val actual =
        intercept[FileFormatException] {
            val actualDF: DataFrame = operator.transformColumns(df, columnSeq, encryptor.encrypt(_))
            }
        val expected = "Columns cannot be empty"
        //Assert
        assert(actual.getMessage == expected)
    }


    "getSchema" should "return the schema for a valid dataframe" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        import session.implicits._
        val df = Seq(
            (8, "bat"),
            (64, "mouse"),
            (-27, "horse")
            ).toDF("number", "word")
        val expected = Seq(
            "number",
            "word"
        )

        //Act
        val actual = operator.getSchema(df)

        //Assert
        assert(actual.equals(expected))
    }
    


    "renameColumns" should "return a dataframe with renamed columns when receive valid dataframe and columns" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        import session.implicits._
        val df = Seq(
            (8, "bat"),
            (64, "mouse"),
            (-27, "horse")
            ).toDF("number", "word")
        val expected = Seq(
            "number",
            "word"
        )
        val columns: Map[String, String]  = Map(
                                                "number" -> "number_GRP",
                                                "word" -> "word_GRP")

        val expectedSchema: Seq[String] = columns.values.toSeq
        //Act
        val actual = operator.renameColumns(df, columns)


        //Assert
        val actualSchema: Seq[String] = this.getSchema(actual)

        assert(actualSchema.equals(expectedSchema))
    }
    


    it should "return FileFormatException when receive empty columns" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        import session.implicits._
        val df = Seq(
            (8, "bat"),
            (64, "mouse"),
            (-27, "horse")
            ).toDF("number", "word")
        
        val columns: Map[String, String]  = Map()
        //Act
        val actual =
        intercept[FileFormatException] {
            val data = operator.renameColumns(df, columns)
        }
        val expected = "Columns cannot be empty"

        //Assert
        assert(actual.getMessage == expected)
    }

    "copyColumns" should "return a dataframe with copied columns when receive valid dataframe and columns" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        import session.implicits._
        val df = Seq(
            ("8", "bat"),
            ("64", "mouse"),
            ("-27", "horse")
            ).toDF("number", "word")

        val columns: Map[String, String]  = Map(
                                                "number" -> "number_COPY",
                                                "word" -> "word_COPY")

        val expectedSchema: Seq[String] = columns.values.toSeq
        //Act
        val actual = operator.copyColumns(df, columns)
        val expectedCopiedNumber = df.select("number").map( x => x.getString(0)).collect.toSeq
        val expectedCopiedWord = df.select("word").map( x => x.getString(0)).collect.toSeq

        val actualCopiedNumber = actual.select("number_COPY").map( x => x.getString(0)).collect.toSeq
        val actualCopiedWord = actual.select("word_COPY").map( x => x.getString(0)).collect.toSeq

        //Assert

        assert(actualCopiedNumber.equals(expectedCopiedNumber) && actualCopiedWord.equals(expectedCopiedWord))
    }
    
    it should "return a dataframe with null column when receive valid dataframe and a column from columns does not exist" in {

        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        import session.implicits._
        val df = Seq(
            ("8", "bat"),
            ("64", "mouse"),
            ("-27", "horse")
            ).toDF("number", "word")

        val columns: Map[String, String]  = Map(
                                                "number" -> "number_COPY",
                                                "nonecolumn" -> "nonecolumn_COPY")

        val expectedSchema: Seq[String] = columns.values.toSeq

        val expectedDf = Seq(
            ("8", null),
            ("64", null),
            ("-27", null)
            ).toDF("number", "nonecolumn")

        //Act
        val actual = operator.copyColumns(df, columns)
        val expectedCopiedNumber: Seq[String] = expectedDf.select("number").map( x => x.getString(0)).collect.toSeq
        val expectedCopiedNoneColumn: Seq[String] = expectedDf.select("nonecolumn").map( x => x.getString(0)).collect.toSeq

        val actualCopiedNumber: Seq[String] = actual.select("number_COPY").map( x => x.getString(0)).collect.toSeq
        val actualCopiedNoneColumn: Seq[String] = actual.select("nonecolumn_COPY").map( x => x.getString(0)).collect.toSeq

        //Assert
        assert(actualCopiedNumber.equals(expectedCopiedNumber) && actualCopiedNoneColumn.equals(expectedCopiedNoneColumn))

    }


    it should "return FileFormatException when receive empty columns" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        import session.implicits._
        val df = Seq(
            (8, "bat"),
            (64, "mouse"),
            (-27, "horse")
            ).toDF("number", "word")
        
        val columns: Map[String, String]  = Map()
        //Act
        val actual =
        intercept[FileFormatException] {
            val data = operator.copyColumns(df, columns)
        }
        val expected = "Columns cannot be empty"

        //Assert
        assert(actual.getMessage == expected)
    }


    "concatWordsToColumn" should "return a concated column with words in a dataframe when receive valid column and words" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        import session.implicits._
        val df = Seq(
            ("8", "bat"),
            ("64", "mouse"),
            ("-27", "horse")
            ).toDF("number", "word")
        val words = Seq("CL", "01")
        val column = "number"
        val expectedDf = Seq(
            ("CL018", "bat"),
            ("CL0164", "mouse"),
            ("CL01-27", "horse")
            ).toDF("number", "word")

        //Act
        val actual = operator.concatWordsToColumn(df, column, words)
        val expectedConcatedNumber = expectedDf.select("number").map( x => x.getString(0)).collect.toSeq

        val actualConcatedNumber = actual.select("number").map( x => x.getString(0)).collect.toSeq

        //Assert

        assert(expectedConcatedNumber.equals(actualConcatedNumber))
    }

    it should "return FileFormatException when receive empty column" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        import session.implicits._
        val df = Seq(
            (8, "bat"),
            (64, "mouse"),
            (-27, "horse")
            ).toDF("number", "word")
        
        val column = ""
        val words = Seq("CL", "01")

        //Act
        val actual =
        intercept[FileFormatException] {
            val data = operator.concatWordsToColumn(df, column, words)
        }
        val expected = "Column cannot be empty"

        //Assert
        assert(actual.getMessage == expected)
    }

    it should "return FileFormatException when receive null column" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        import session.implicits._
        val df = Seq(
            (8, "bat"),
            (64, "mouse"),
            (-27, "horse")
            ).toDF("number", "word")
        
        val column = null
        val words = Seq("CL", "01")

        //Act
        val actual =
        intercept[FileFormatException] {
            val data = operator.concatWordsToColumn(df, column, words)
        }
        val expected = "Column cannot be null"

        //Assert
        assert(actual.getMessage == expected)
    }

    it should "return FileFormatException when receive empty words" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        import session.implicits._
        val df = Seq(
            (8, "bat"),
            (64, "mouse"),
            (-27, "horse")
            ).toDF("number", "word")
        
        val column = "number"
        val words = Seq()

        //Act
        val actual =
        intercept[FileFormatException] {
            val data = operator.concatWordsToColumn(df, column, words)
        }
        val expected = "Words cannot be empty"

        //Assert
        assert(actual.getMessage == expected)
    }

    it should "return FileFormatException when receive not existing column" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        import session.implicits._
        val df = Seq(
            (8, "bat"),
            (64, "mouse"),
            (-27, "horse")
            ).toDF("number", "word")
        
        val column = "nonexistingcolumn"
        val words = Seq("CL", "01")

        //Act
        val actual =
        intercept[FileFormatException] {
            val data = operator.concatWordsToColumn(df, column, words)
        }
        val expected = "error concating words to the column"

        //Assert
        assert(actual.getMessage == expected)
    }

    "createColumnWithSingleValue" should "return a new column with single value in a dataframe when receive valid column and value" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        import session.implicits._
        val df = Seq(
            ("8", "bat"),
            ("64", "mouse"),
            ("-27", "horse")
            ).toDF("number", "word")
        val value = "new"
        val column = "newColumn"
        val expectedDf = Seq(
            ("8", "bat", "new"),
            ("64", "mouse", "new"),
            ("-27", "horse", "new")
            ).toDF("number", "word", "newColumn")

        //Act
        val actual = operator.createColumnWithSingleValue(df, column, value)
        val expectedNewColumn = expectedDf.select("newColumn").map( x => x.getString(0)).collect.toSeq

        val actualNewColumn = actual.select("newColumn").map( x => x.getString(0)).collect.toSeq

        //Assert

        assert(expectedNewColumn.equals(actualNewColumn))
    }

    it should "return FileFormatException when receive empty column" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        import session.implicits._
        val df = Seq(
            ("8", "bat"),
            ("64", "mouse"),
            ("-27", "horse")
            ).toDF("number", "word")
        val value = "new"
        val column = ""

        //Act
        val actual =
        intercept[FileFormatException] {
            val data = operator.createColumnWithSingleValue(df, column, value)
        }
        val expected = "Column cannot be empty"

        //Assert
        assert(actual.getMessage == expected)
    }

    it should "return FileFormatException when receive null column" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        import session.implicits._
        val df = Seq(
            ("8", "bat"),
            ("64", "mouse"),
            ("-27", "horse")
            ).toDF("number", "word")
        val value = "new"
        val column = null

        //Act
        val actual =
        intercept[FileFormatException] {
            val data = operator.createColumnWithSingleValue(df, column, value)
        }
        val expected = "Column cannot be null"

        //Assert
        assert(actual.getMessage == expected)
    }


    it should "return a new column with null value in a dataframe when receive valid column and empty value" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        import session.implicits._
        val df = Seq(
            ("8", "bat"),
            ("64", "mouse"),
            ("-27", "horse")
            ).toDF("number", "word")
        val value = ""
        val column = "newColumn"
        val expectedDf = Seq(
            ("8", "bat", null),
            ("64", "mouse", null),
            ("-27", "horse", null)
            ).toDF("number", "word", "newColumn")

        //Act
        val actual = operator.createColumnWithSingleValue(df, column, value)
        val expectedNewColumn = expectedDf.select("newColumn").map( x => x.getString(0)).collect.toSeq

        val actualNewColumn = actual.select("newColumn").map( x => x.getString(0)).collect.toSeq

        //Assert

        assert(expectedNewColumn.equals(actualNewColumn))
    }

    it should "return a new column with null value in a dataframe when receive valid column and null value" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        import session.implicits._
        val df = Seq(
            ("8", "bat"),
            ("64", "mouse"),
            ("-27", "horse")
            ).toDF("number", "word")
        val value = null
        val column = "newColumn"
        val expectedDf = Seq(
            ("8", "bat", null),
            ("64", "mouse", null),
            ("-27", "horse", null)
            ).toDF("number", "word", "newColumn")
            
        //Act
        val actual = operator.createColumnWithSingleValue(df, column, value)
        val expectedNewColumn = expectedDf.select("newColumn").map( x => x.getString(0)).collect.toSeq

        val actualNewColumn = actual.select("newColumn").map( x => x.getString(0)).collect.toSeq

        //Assert

        assert(expectedNewColumn.equals(actualNewColumn))
    }

    "createDateColumns" should "return a date columns in a dataframe when receive valid DataFrame and column" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val column = "date"
        import session.implicits._
        val df = Seq(
            (8, "bat", "2020-10-01"),
            (64, "mouse", "2020-10-02"),
            (-27, "horse", "2020-10-03")
            ).toDF("number", "word", "date")

        val expectedSchema = Seq("number", "word", "date", "year", "month", "day")


        //Act
        val actual = operator.createDateColumns(df, column)
        val actualSchema = this.getSchema(actual)
        //Assert

        assert(actualSchema.equals(expectedSchema))
    }

    it should "return FileFormatException when receive null column" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val column = null
        import session.implicits._
        val df = Seq(
            (8, "bat", "2020-10-01"),
            (64, "mouse", "2020-10-02"),
            (-27, "horse", "2020-10-03")
            ).toDF("number", "word", "date")

        //Act
        val actual =
        intercept[FileFormatException] {
            val data = operator.createDateColumns(df, column)
        }
        val expected = "Column cannot be null"

        //Assert
        assert(actual.getMessage == expected)
    }

    it should "return FileFormatException when receive empty column" in {
        //Arrange
        val sessionFactory = new SparkSessionFactory()
        val session = sessionFactory.makeLocal()
        val operator: FileFormatOperator = new FileFormatOperator(session)
        val column = ""
        import session.implicits._
        val df = Seq(
            (8, "bat", "2020-10-01"),
            (64, "mouse", "2020-10-02"),
            (-27, "horse", "2020-10-03")
            ).toDF("number", "word", "date")

        //Act
        val actual =
        intercept[FileFormatException] {
            val data = operator.createDateColumns(df, column)
        }
        val expected = "Column cannot be empty"

        //Assert
        assert(actual.getMessage == expected)
    }
}


    


