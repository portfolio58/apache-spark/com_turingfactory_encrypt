package com.turingfactory.adapters
import com.turingfactory.models._
import com.turingfactory.ports._
import com.turingfactory.exceptions._
import org.scalatest.Assertions._
import org.scalatest.FlatSpec
import org.scalatest.BeforeAndAfter
import org.scalamock.scalatest.MockFactory

class TestSpringEncryptor extends FlatSpec with BeforeAndAfter {

    "constructor" should "create a valid object when receive valid password and salt" in {
        
        //Arrange
        val password: String = "t3stpwd_"
        val salt: String = "07c92cfa3744bcdb" // test value
        //Act
        val encryptor = new SpringEncryptor(password, salt)
        //Assert
        assert(encryptor.isInstanceOf[SpringEncryptor])
    }
    it should "return an EncryptorException when receive empty password and valid salt" in {
        //Arrange
        val password: String = ""
        val salt: String = "07c92cfa3744bcdb" // test value
        //Act
        val actual =
        intercept[EncryptorException] {
             val encryptor = new SpringEncryptor(password, salt)
        }
        val expected = "password cannot be empty"
        //Assert
        assert(actual.getMessage == expected)
    }
    it should "return an EncryptorException when receive null password and valid salt" in {
        //Arrange
        val password: String = null
        val salt: String = "07c92cfa3744bcdb" // test value
        //Act
        val actual =
        intercept[EncryptorException] {
             val encryptor = new SpringEncryptor(password, salt)
        }
        val expected = "password cannot be null"
        //Assert
        assert(actual.getMessage == expected)
    }
    it should "return an EncryptorException when receive valid password and empty salt" in {
        //Arrange
        val password: String = "t3stpwd_"
        val salt: String = "" // test value
        //Act
        val actual =
        intercept[EncryptorException] {
             val encryptor = new SpringEncryptor(password, salt)
        }
        val expected = "salt cannot be empty"
        //Assert
        assert(actual.getMessage == expected)
    }
    it should "return an EncryptorException when receive valid password and null salt" in {
        //Arrange
        val password: String = "t3stpwd_"
        val salt: String = null // test value
        //Act
        val actual =
        intercept[EncryptorException] {
             val encryptor = new SpringEncryptor(password, salt)
        }
        val expected = "salt cannot be null"
        //Assert
        assert(actual.getMessage == expected)
    }

    "encrypt" should "return encrypted String when receive valid text" in {
        
        //Arrange
        val password: String = "t3stpwd_"
        val salt: String = "07c92cfa3744bcdb" // test value
        val text: String = "dummy"
        val expected: String = "5663ed018be13f62de8330e25a88801e"
        val encryptor: Encryptor = new SpringEncryptor(password, salt)
        //Act
        val actual: String = encryptor.encrypt(text)
        
        //Assert
        assert(actual == expected)
    }
    it should "return an EncryptorException when receive empty text" in {
        //Arrange
        val password: String = "t3stpwd_"
        val salt: String = "07c92cfa3744bcdb" // test value
        val text: String = ""
        val encryptor: Encryptor = new SpringEncryptor(password, salt)
        //Act
        val actual =
        intercept[EncryptorException] {
               val encryptedText: String = encryptor.encrypt(text)
        }
        val expected = "text cannot be empty"
        //Assert
        assert(actual.getMessage == expected)
    }
    it should "return an EncryptorException when receive null text" in {
        
        //Arrange
        val password: String = "t3stpwd_"
        val salt: String = "07c92cfa3744bcdb" // test value
        val text: String = null
        val encryptor: Encryptor = new SpringEncryptor(password, salt)
        //Act
        val actual =
        intercept[EncryptorException] {
               val encryptedText: String = encryptor.encrypt(text)
        }
        val expected = "text cannot be null"
        //Assert
        assert(actual.getMessage == expected)
    }
    "decrypt" should "return encrypted String when receive valid text" in {
        
        //Arrange
        val password: String = "t3stpwd_"
        val salt: String = "07c92cfa3744bcdb" // test value
        val text: String = "5663ed018be13f62de8330e25a88801e"
        val expected: String = "dummy"
        val encryptor: Encryptor = new SpringEncryptor(password, salt)
        //Act
        val actual: String = encryptor.decrypt(text)
        
        //Assert
        assert(actual == expected)
    }
    it should "return an EncryptorException when receive empty encryptedText" in {
        
        //Arrange
        val password: String = "t3stpwd_"
        val salt: String = "07c92cfa3744bcdb" // test value
        val text: String = ""
        val encryptor: Encryptor = new SpringEncryptor(password, salt)
        //Act
        val actual =
        intercept[EncryptorException] {
               val decryptedText: String = encryptor.decrypt(text)
        }
        val expected = "encryptedText cannot be empty"
        //Assert
        assert(actual.getMessage == expected)
    }
    it should "return an EncryptorException when receive null encryptedText" in {
        //Arrange
        val password: String = "t3stpwd_"
        val salt: String = "07c92cfa3744bcdb" // test value
        val text: String = null
        val encryptor: Encryptor = new SpringEncryptor(password, salt)
        //Act
        val actual =
        intercept[EncryptorException] {
               val decryptedText: String = encryptor.decrypt(text)
        }
        val expected = "encryptedText cannot be null"
        //Assert
        assert(actual.getMessage == expected)
    }

}
