package com.turingfactory.adapters
import com.turingfactory.models._
import com.turingfactory.ports._
import com.turingfactory.exceptions._
import org.scalatest.Assertions._
import org.scalatest.FlatSpec
import org.scalatest.BeforeAndAfter
import org.scalamock.scalatest.MockFactory

class TestSha2Encryptor extends FlatSpec with BeforeAndAfter {

    "constructor" should "create a valid object when receive valid salt" in {
        
        //Arrange
        val salt: String = "salt" // test value
        //Act
        val encryptor = new Sha2Encryptor(salt)
        //Assert
        assert(encryptor.isInstanceOf[Sha2Encryptor])
    }

    it should "return an EncryptorException when receive empty salt" in {
        //Arrange
        val salt: String = "" // test value
        //Act
        val actual =
        intercept[EncryptorException] {
            val encryptor = new Sha2Encryptor(salt)
        }
        val expected = "salt cannot be empty"
        //Assert
        assert(actual.getMessage == expected)
    }

    it should "return an EncryptorException when receive null salt" in {
        //Arrange
        val salt: String = null // test value
        //Act
        val actual =
        intercept[EncryptorException] {
             val encryptor = new Sha2Encryptor(salt)
        }
        val expected = "salt cannot be null"
        //Assert
        assert(actual.getMessage == expected)
    }


    "encrypt" should "return encrypted String when receive valid text" in {
        
        //Arrange
        val salt: String = "salt" // test value
        val text: String = "dummy"
        val expected: String = "130233F4CFD66733816D54F3C5961F286A273343113F4FC925027FB08659FA10"
        val encryptor = new Sha2Encryptor(salt)
        //Act
        val actual: String = encryptor.encrypt(text)
        
        //Assert
        assert(actual == expected)
    }

    it should "return an EncryptorException when receive empty text" in {
        //Arrange
        val salt: String = "salt" // test value
        val text: String = ""
        val encryptor = new Sha2Encryptor(salt)
        //Act
        val actual =
        intercept[EncryptorException] {
               val encryptedText: String = encryptor.encrypt(text)
        }
        val expected = "text cannot be empty"
        //Assert
        assert(actual.getMessage == expected)
    }
    it should "return an EncryptorException when receive null text" in {
        
        //Arrange
        val salt: String = "salt" // test value
        val text: String = null
        val encryptor = new Sha2Encryptor(salt)
        //Act
        val actual =
        intercept[EncryptorException] {
               val encryptedText: String = encryptor.encrypt(text)
        }
        val expected = "text cannot be null"
        //Assert
        assert(actual.getMessage == expected)
    }
    

}
