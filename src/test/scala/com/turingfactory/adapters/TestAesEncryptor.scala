package com.turingfactory.adapters
import com.turingfactory.models._
import com.turingfactory.ports._
import com.turingfactory.exceptions._
import org.scalatest.Assertions._
import org.scalatest.FlatSpec
import org.scalatest.BeforeAndAfter
import org.scalamock.scalatest.MockFactory

class TestAesEncryptor extends FlatSpec with BeforeAndAfter {

    "constructor" should "create a valid object when receive valid key and iv" in {
        
        //Arrange
        val key: String = "OIiqKc0ddjcBMMLfDx/dpQ=="
        val iv: String = "6AQuAnuFa/WIGSp5n5O7VA==" // test value
        //Act
        val encryptor = new AesEncryptor(key, iv)
        //Assert
        assert(encryptor.isInstanceOf[AesEncryptor])
    }

    it should "return an EncryptorException when receive empty key and valid iv" in {
        //Arrange
        val key: String = ""
        val iv: String = "6AQuAnuFa/WIGSp5n5O7VA==" // test value
        //Act
        val actual =
        intercept[EncryptorException] {
             val encryptor = new AesEncryptor(key, iv)
        }
        val expected = "key cannot be empty"
        //Assert
        assert(actual.getMessage == expected)
    }

    it should "return an EncryptorException when receive null key and valid iv" in {
        //Arrange
        val key: String = null
        val iv: String = "6AQuAnuFa/WIGSp5n5O7VA==" // test value
        //Act
        val actual =
        intercept[EncryptorException] {
             val encryptor = new AesEncryptor(key, iv)
        }
        val expected = "key cannot be null"
        //Assert
        assert(actual.getMessage == expected)
    }

    it should "return an EncryptorException when receive valid key and empty iv" in {
        //Arrange
        val key: String = "OIiqKc0ddjcBMMLfDx/dpQ=="
        val iv: String = "" // test value
        //Act
        val actual =
        intercept[EncryptorException] {
             val encryptor = new AesEncryptor(key, iv)
        }
        val expected = "iv cannot be empty"
        //Assert
        assert(actual.getMessage == expected)
    }

    it should "return an EncryptorException when receive valid key and null iv" in {
        //Arrange
        val key: String = "OIiqKc0ddjcBMMLfDx/dpQ=="
        val iv: String = null // test value
        //Act
        val actual =
        intercept[EncryptorException] {
             val encryptor = new AesEncryptor(key, iv)
        }
        val expected = "iv cannot be null"
        //Assert
        assert(actual.getMessage == expected)
    }

    it should "return an EncryptorException when receive valid key and invalid iv" in {
        //Arrange
        val key: String = "OIiqKc0ddjcBMMLfDx/dpQ=="
        val iv: String = "ASD" // test value
        //Act
        val actual =
        intercept[EncryptorException] {
             val encryptor = new AesEncryptor(key, iv)
        }
        val expected = "invalid algorithmParameterSpec"
        //Assert
        assert(actual.getMessage == expected)
    }

    it should "return an EncryptorException when receive valid invalid key and valid iv" in {
        //Arrange
        val key: String = "ASD"
        val iv: String = "6AQuAnuFa/WIGSp5n5O7VA==" // test value
        //Act
        val actual =
        intercept[EncryptorException] {
             val encryptor = new AesEncryptor(key, iv)
        }
        val expected = "invalid secretKey"
        //Assert
        assert(actual.getMessage == expected)
    }

    "encrypt" should "return encrypted String when receive valid text" in {
        
        //Arrange
        val key: String = "OIiqKc0ddjcBMMLfDx/dpQ=="
        val iv: String = "6AQuAnuFa/WIGSp5n5O7VA==" // test value
        val text: String = "dummy"
        val expected: String = "70553104241f4b185fca6194e896e630"
        val encryptor: Encryptor = new AesEncryptor(key, iv)
        //Act
        val actual: String = encryptor.encrypt(text)
        //Assert
        assert(actual == expected)
    }

    it should "return an EncryptorException when receive empty text" in {
        //Arrange
        val key: String = "OIiqKc0ddjcBMMLfDx/dpQ=="
        val iv: String = "6AQuAnuFa/WIGSp5n5O7VA==" // test value
        val text: String = ""
        val encryptor: Encryptor = new AesEncryptor(key, iv)
        //Act
        val actual =
        intercept[EncryptorException] {
               val encryptedText: String = encryptor.encrypt(text)
        }
        val expected = "text cannot be empty"
        //Assert
        assert(actual.getMessage == expected)
    }

    it should "return an EncryptorException when receive null text" in {
        
        //Arrange
        val key: String = "OIiqKc0ddjcBMMLfDx/dpQ=="
        val iv: String = "6AQuAnuFa/WIGSp5n5O7VA==" // test value
        val text: String = null
        val encryptor: Encryptor = new AesEncryptor(key, iv)
        //Act
        val actual =
        intercept[EncryptorException] {
               val encryptedText: String = encryptor.encrypt(text)
        }
        val expected = "text cannot be null"
        //Assert
        assert(actual.getMessage == expected)
    }

    "encryptToB64" should "return encrypted String when receive valid text" in {
        
        //Arrange
        val key: String = "OIiqKc0ddjcBMMLfDx/dpQ=="
        val iv: String = "6AQuAnuFa/WIGSp5n5O7VA==" // test value
        val text: String = "dummy"
        val expected: String = "cFUxBCQfSxhfymGU6JbmMA=="
        val encryptor: Encryptor = new AesEncryptor(key, iv)
        //Act
        val actual: String = encryptor.encryptToB64(text)
        //Assert
        assert(actual == expected)
    }


    it should "return an EncryptorException when receive empty text" in {
        //Arrange
        val key: String = "OIiqKc0ddjcBMMLfDx/dpQ=="
        val iv: String = "6AQuAnuFa/WIGSp5n5O7VA==" // test value
        val text: String = ""
        val encryptor: Encryptor = new AesEncryptor(key, iv)
        //Act
        val actual =
        intercept[EncryptorException] {
               val encryptedText: String = encryptor.encrypt(text)
        }
        val expected = "text cannot be empty"
        //Assert
        assert(actual.getMessage == expected)
    }

    it should "return an EncryptorException when receive null text" in {
        
        //Arrange
        val key: String = "OIiqKc0ddjcBMMLfDx/dpQ=="
        val iv: String = "6AQuAnuFa/WIGSp5n5O7VA==" // test value
        val text: String = null
        val encryptor: Encryptor = new AesEncryptor(key, iv)
        //Act
        val actual =
        intercept[EncryptorException] {
               val encryptedText: String = encryptor.encrypt(text)
        }
        val expected = "text cannot be null"
        //Assert
        assert(actual.getMessage == expected)
    }


    "decrypt" should "return encrypted String when receive valid text" in {
        
        //Arrange
        val key: String = "OIiqKc0ddjcBMMLfDx/dpQ=="
        val iv: String = "6AQuAnuFa/WIGSp5n5O7VA==" // test value
        val text: String = "70553104241f4b185fca6194e896e630"
        val expected: String = "dummy"
        val encryptor: Encryptor = new AesEncryptor(key, iv)
        //Act
        val actual: String = encryptor.decrypt(text)
        
        //Assert
        assert(actual == expected)
    }

    it should "return an EncryptorException when receive empty encryptedText" in {
        
        //Arrange
        val key: String = "OIiqKc0ddjcBMMLfDx/dpQ=="
        val iv: String = "6AQuAnuFa/WIGSp5n5O7VA==" // test value
        val text: String = ""
        val encryptor: Encryptor = new AesEncryptor(key, iv)
        //Act
        val actual =
        intercept[EncryptorException] {
               val decryptedText: String = encryptor.decrypt(text)
        }
        val expected = "encryptedText cannot be empty"
        //Assert
        assert(actual.getMessage == expected)
    }

    it should "return an EncryptorException when receive null encryptedText" in {
        //Arrange
        val key: String = "OIiqKc0ddjcBMMLfDx/dpQ=="
        val iv: String = "6AQuAnuFa/WIGSp5n5O7VA==" // test value
        val text: String = null
        val encryptor: Encryptor = new AesEncryptor(key, iv)
        //Act
        val actual =
        intercept[EncryptorException] {
               val decryptedText: String = encryptor.decrypt(text)
        }
        val expected = "encryptedText cannot be null"
        //Assert
        assert(actual.getMessage == expected)
    }

}
