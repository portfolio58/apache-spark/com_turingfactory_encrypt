package com.turingfactory.exceptions

case class ParserException(message: String) extends Exception(message)