package com.turingfactory.exceptions

case class EncoderException(message: String) extends Exception(message)
