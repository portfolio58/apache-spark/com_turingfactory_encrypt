package com.turingfactory.exceptions

case class EncryptorException(message: String) extends Exception(message)