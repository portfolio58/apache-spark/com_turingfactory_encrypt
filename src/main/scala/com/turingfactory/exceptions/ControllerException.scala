package com.turingfactory.exceptions

case class ControllerException(message: String) extends Exception(message)
