package com.turingfactory.exceptions

case class FileFormatException(message: String) extends Exception(message)