package com.turingfactory.exceptions

case class TableException(message: String) extends Exception(message)