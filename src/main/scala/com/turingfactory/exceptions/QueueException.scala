package com.turingfactory.exceptions

case class QueueException(message: String) extends Exception(message)