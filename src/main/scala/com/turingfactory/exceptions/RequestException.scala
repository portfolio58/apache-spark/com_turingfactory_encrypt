package com.turingfactory.exceptions

case class RequestException(message: String) extends Exception(message)