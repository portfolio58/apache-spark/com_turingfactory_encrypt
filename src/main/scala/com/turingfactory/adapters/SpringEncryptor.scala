package com.turingfactory.adapters
import com.turingfactory.ports.Encryptor
import com.turingfactory.exceptions.EncryptorException
import org.springframework.security.crypto.encrypt.Encryptors
import org.springframework.security.crypto.encrypt.TextEncryptor

class SpringEncryptor extends Encryptor with Serializable {
    var textEncriptor: TextEncryptor = _
    def this(password: String, salt: String){
        this()
        if (password == null){
            throw new EncryptorException("password cannot be null")
        }
        else{
            if (password.isEmpty){
                throw new EncryptorException("password cannot be empty")
            }
            else {
                if (salt == null){
                    throw new EncryptorException("salt cannot be null")
                }
                else{
                    if (salt.isEmpty){
                        throw new EncryptorException("salt cannot be empty")
                    }
                    else{
                        @transient lazy val tmpencryptor = Encryptors.queryableText(password, salt)
                        this.textEncriptor = tmpencryptor
                    }
                }
            }
        }
    }
    def encrypt(text: String): String = {
        if (text == null){
            // return "null"
            throw new EncryptorException("text cannot be null")
        }
        else{
            if (text.isEmpty){
                // return ""
                throw new EncryptorException("text cannot be empty")
            }
            else {
                 return this.textEncriptor.encrypt(text)
            }
        }
       
    }
    def encryptToB64(text: String): String = {
        return "not implemented"
    }
    
    def decrypt(encryptedText: String): String = {

        if (encryptedText == null){
            // return "null"
            throw new EncryptorException("encryptedText cannot be null")
        }
        else{
            if (encryptedText.isEmpty){
                // return ""
                throw new EncryptorException("encryptedText cannot be empty")
            }
            else {
                this.textEncriptor.decrypt(encryptedText)
            }
        }
    }
}
