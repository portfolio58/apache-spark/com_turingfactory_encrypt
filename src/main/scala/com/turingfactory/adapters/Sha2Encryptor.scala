package com.turingfactory.adapters

import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.io.UnsupportedEncodingException
import com.turingfactory.exceptions.EncryptorException
import com.turingfactory.ports.Encryptor
import java.sql.SQLException
import oracle.sql.RAW

class Sha2Encryptor extends Encryptor with Serializable{

	private val algorithm: String  = "SHA-256"
	private val encoding: String  = "UTF-8"
	private var salt: String = null

    def this(salt: String){
        this()
        validateSalt(salt)
        this.salt = salt
    }
    private def validateSalt(salt: String): Boolean = {
        salt match {
        case null => throw new EncryptorException("salt cannot be null")
        case salt if salt.isEmpty => throw new EncryptorException("salt cannot be empty")
        case _ => true
        }
    }
    
    private def validateText(text: String): Boolean = {
        text match {
        case null => throw new EncryptorException("text cannot be null")
        case text if text.isEmpty => throw new EncryptorException("text cannot be empty")
        case _ => true
        } 
    }

    def encrypt(text: String): String = {
        validateText(text)
        try{
            return RAW.newRAW(MessageDigest.getInstance(this.algorithm).digest(text.concat(this.salt).getBytes(this.encoding))).stringValue()
        } catch{
            case e: SQLException =>
            {
                throw new EncryptorException("error creating RAW")
            }
            case e: NoSuchAlgorithmException =>
            {
                throw new EncryptorException("no such algorithm supported")
            }
            case e: UnsupportedEncodingException =>
            {
                throw new EncryptorException("no such encoding supported")
            }
        }
    }
        
    def encryptToB64(text: String): String = {
        return "NOT IMPLEMENTED"
    }

    def decrypt(encryptedText: String): String = {
        return "NOT IMPLEMENTED"
    }

}