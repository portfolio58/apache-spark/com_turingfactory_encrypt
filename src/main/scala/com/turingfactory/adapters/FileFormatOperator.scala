package com.turingfactory.adapters
import com.turingfactory.exceptions.FileFormatException
import com.typesafe.scalalogging.Logger
import org.apache.spark.sql.{DataFrame, SparkSession, Dataset, Column, Row, SaveMode}
import org.apache.spark.sql.functions.{col, when, lit, concat, date_format}
import scala.collection.mutable.ListBuffer
import org.apache.spark.sql.types.{StructType, StructField, StringType, IntegerType, DataType}
import org.apache.hadoop.fs.{FileSystem, Path}

class FileFormatOperator(val session: SparkSession) extends Serializable {

    val sessionErrorMsg = "Session cannot be null"
    val pathNullMsg = "Path cannot be null"
    val pathEmptyMsg = "Path cannot be empty"
    val dataFrameEmptyMsg = "DataFrame cannot be empty"
    val dataFrameNullMsg = "DataFrame cannot be null"
    val columnsEmptyMsg = "Columns cannot be empty"
    val queryNullMsg = "Query cannot be null"
    val queryEmptyMsg = "Query cannot be empty"
    val aliasNullMsg = "Alias cannot be null"
    val aliasEmptyMsg = "Alias cannot be empty"
    val wordsEmptyMsg = "Words cannot be empty"
    val fileFormatNullMsg = "fileFormat cannot be null"
    val fileFormatEmptyMsg = "fileFormat cannot be empty"
    val modeNullMsg = "mode cannot be null"
    val modeEmptyMsg = "mode cannot be empty"
    val partitionedByNullMsg = "partitionedBy cannot be null"
    val partitionedByEmptyMsg = "partitionedBy cannot be empty"
    val headerNullMsg = "header cannot be null"
    val headerEmptyMsg = "header cannot be empty"
    //val fileFormatNotImplementedMsg = s"fileFormat: $fileFormat not Implemented"
    val mylog = Logger("FileFormatOperator")
    
    private def isEmpty(df: DataFrame): Boolean = {
        @transient lazy val validateEmptyDf = df.rdd.isEmpty
        return validateEmptyDf
    }

    private def validatePath(path: String): Boolean = {
        path match {
            case null => throw new FileFormatException(pathNullMsg)
            case path if path.isEmpty => throw new FileFormatException(pathEmptyMsg)
            case _ => return true
        }     
    }
    
    private def validatePartitionedBy(partitionedBy: Seq[String]): Boolean = {
        partitionedBy match {
            case null => throw new FileFormatException(partitionedByNullMsg)
            case partitionedBy if partitionedBy.isEmpty => throw new FileFormatException(partitionedByEmptyMsg)
            case _ => return true
        }     
    }
    private def validateSession(): Boolean = {
        this.session match {
            case null => throw new FileFormatException(sessionErrorMsg)
            case _ => return true
        }     
    }

    private def validateColumn(column: String): Boolean = {
        column match {
            case null => throw new FileFormatException("Column cannot be null")
            case column if column.isEmpty => throw new FileFormatException("Column cannot be empty")
            case _ => return true
        }     
    }

    // private def validateValue(value: String): Boolean = {
    //     value match {
    //         case null => throw new FileFormatException("Value cannot be null")
    //         case value if value.isEmpty => throw new FileFormatException("Value cannot be empty")
    //         case _ => return true
    //     }     
    // }

    private def validateColumns(columns : Seq[String]): Boolean = {
        columns match {
            case columns if columns.isEmpty => throw new FileFormatException(columnsEmptyMsg)
            case _ => return true
        }     
    }

    private def validateMColumns(columns : scala.collection.immutable.Map[String, String]): Boolean = {
        columns match {
            case columns if columns.isEmpty => throw new FileFormatException(columnsEmptyMsg)
            case _ => return true
        }  
    }

    private def validateDcolumns(columns: scala.collection.immutable.Map[Column, DataType]): Boolean = {
         columns match {
            case columns if columns.isEmpty => throw new FileFormatException(columnsEmptyMsg)
            case _ => return true
        }         
    }

    private def validateTable(table: DataFrame): Boolean = {
        table match {
        case null => throw new FileFormatException(dataFrameNullMsg)
        case table if this.isEmpty(table) => throw new FileFormatException(dataFrameEmptyMsg)
        case table if !this.isEmpty(table) => return true
        }
    }

    private def validateQuery(query: String): Boolean = {
        query match {
            case null => throw new FileFormatException(queryNullMsg)
            case query if query.isEmpty => throw new FileFormatException(queryEmptyMsg)
            case _ => return true
        }     
    }

    private def validateAlias(alias: String): Boolean = {
        alias match {
            case null => throw new FileFormatException(aliasNullMsg)
            case alias if alias.isEmpty => throw new FileFormatException(aliasEmptyMsg)
            case _ => return true
        }     
    }


    private def validateWords(words : Seq[String]): Boolean = {
        words match {
            case words if words.isEmpty => throw new FileFormatException(wordsEmptyMsg)
            case _ => return true
        }     
    }

    private def validateFileFormat(fileFormat : String): Boolean = {
        fileFormat match {
            case null => throw new FileFormatException(fileFormatNullMsg)
            case fileFormat if fileFormat.isEmpty => throw new FileFormatException(fileFormatEmptyMsg)
            case _ => return true
        }     
    }


    private def validateMode(mode : String): Boolean = {
        mode match {
            case null => throw new FileFormatException(modeNullMsg)
            case mode if mode.isEmpty => throw new FileFormatException(modeEmptyMsg)
            case _ => return true
        }     
    }


    def getTable(path: String, fileFormat: String, header: Boolean = true): DataFrame = {
        this.validateSession
        this.validatePath(path)
        this.validateFileFormat(fileFormat)
        try{
            fileFormat match {
                case "avro" => {
                    var df = this.session.read.format("avro").load(path)
                    return df
                }
                case "csv" => {
                    var df = this.session.read.format("csv").option("header", header.toString).load(path)
                    return df    
                }
                case _ => throw new FileFormatException(s"fileFormat: $fileFormat not implemented")
            }

        }
        catch{
            case x: Exception =>
                {
                    throw new FileFormatException(x.getMessage)
                }
        }
    }

    def saveTable(path: String, df: DataFrame, mode: String = "overwrite") = {
        this.validatePath(path)
        this.validateMode(mode)
        //this.validateTable(df)
        try{
            mode match {
                case "overwrite" => {
                    mylog.info(f"saving $path")
                    df.write.mode(SaveMode.Overwrite).format("avro").save(path)
                }
                case "append" => {
                    mylog.info(f"saving $path")
                    df.write.mode(SaveMode.Append).format("avro").save(path)
                }
                case _ => throw new FileFormatException(s"mode: $mode not implemented")
            }

        }
        catch{
            case x: Exception =>
            {
                throw new FileFormatException(x.getMessage)
            }
        }
    }


    def savePartitionedTable(path: String, df: DataFrame, partitionedBy: Seq[String], fileFormat: String, header: Boolean = true, mode: String = "append") = {
        this.validatePath(path) 
        this.validatePartitionedBy(partitionedBy)
        this.validateFileFormat(fileFormat)
        this.validateMode(mode)
        //this.validateTable(df)
        try{
            fileFormat match {
                case "avro" => {
                    df.write.partitionBy(partitionedBy: _*).mode(SaveMode.Append).format("avro").save(path)
                }
                case "csv" => {
                    df.write.partitionBy(partitionedBy: _*).mode(SaveMode.Append).format("csv").option("header", header.toString).save(path)
                }
                case _ => throw new FileFormatException(s"fileFormat: $fileFormat not implemented")
            }

            // mode match {
            //     case null => throw new FileFormatException(modeNullMsg)
            //     case mode if mode.isEmpty => throw new FileFormatException(modeEmptyMsg)
            //     case "overwrite" => {
            //         mylog.info(f"saving $path")
            //         df.write.partitionBy(partitionedBy).mode(SaveMode.Overwrite).format("avro").save(path)
            //     }
            //     case "append" => {
            //         mylog.info(f"saving $path")
            //         df.write.partitionBy(partitionedBy).mode(SaveMode.Append).format("avro").save(path)
            //     }
            //     case _ => throw new FileFormatException(s"mode: $mode not implemented")
            // }

        }
        catch{
            case x: Exception =>
            {
                throw new FileFormatException(x.getMessage)
            }
        }
    }


    def renameColumns(df: DataFrame, columns: scala.collection.immutable.Map[String, String]): DataFrame = {
        //this.validateTable(df)
        this.validateMColumns(columns)
            // try {
        var tmpDf = df
        for ((key, value) <- columns) {

            tmpDf = tmpDf.withColumnRenamed(key, value)
            
        }
        return tmpDf
    }


    def copyColumns(df: DataFrame, columns: scala.collection.immutable.Map[String, String]): DataFrame = {
        //this.validateTable(df)
        this.validateMColumns(columns)
        try {
            var tmpDf = df
            for ((key, value) <- columns) {
                if (df.columns.contains(key)){
                    tmpDf = tmpDf.withColumn(value, col(key))
                }
                else{
                    tmpDf = tmpDf.withColumn(value, lit(null).cast(StringType))
                }
            }
            return tmpDf
        }
        catch{
            case x: Exception =>
            {
                throw new FileFormatException("error copying the columns")
            }
        }
    }

    def queryTable(df: DataFrame, query: String, alias: String): DataFrame = {
        this.validateSession 
        //this.validateTable(df) 
        this.validateQuery(query) 
        this.validateAlias(alias)
        try{
            df.createOrReplaceTempView(alias)
            return this.session.sql(query)
        }
        catch{
            case x: Exception =>
                {
                    throw new FileFormatException("error querying the table")
                }
        }
    }


    def castColumns(df: DataFrame, columns: scala.collection.immutable.Map[Column, DataType]): DataFrame = {
        //this.validateTable(df) 
        this.validateDcolumns(columns)
        var tmpDf = df
        for ((key, value) <- columns){
            tmpDf = tmpDf.withColumn(key.toString, tmpDf(key.toString).cast(value))
        }
        return tmpDf
    }

    def existFiles(path: String): Boolean = {
        this.validateSession 
        this.validatePath(path)
        val sc = this.session.sparkContext
        val conf = sc.hadoopConfiguration
        val p: Path  = new Path(path)
        val fs: FileSystem = p.getFileSystem(conf)
        try{
            return fs.exists(p)
        } 
        catch{
            case x: Exception =>
                {
                    throw new FileFormatException("error validating the path")
                }
        }
    }

    def concatWordsToColumn(df: DataFrame, column: String, words: Seq[String]): DataFrame = {
        //this.validateTable(df)
        this.validateColumn(column)
        this.validateWords(words)
        try{
            return df.withColumn(column, concat(lit(words.mkString), col(column)))
        }
        catch{
            case x: Exception =>
                {
                    throw new FileFormatException("error concating words to the column")
                }
        }
    }



    def createColumnWithSingleValue(df: DataFrame, column: String, value: String): DataFrame = {
        //this.validateTable(df)
        this.validateColumn(column)
        try {
            if(value != null && value != "null" && !value.isEmpty){
                return df.withColumn(column, lit(value).cast(StringType))
            }
            else{
                return df.withColumn(column, lit(null).cast(StringType))
            }
        }
        catch {
            case x: Exception =>
                {
                    throw new FileFormatException("error creating the column")
                }    
        }
    }

    def createDateColumns(df: DataFrame, column: String): DataFrame = {
        //this.validateTable(df)
        this.validateColumn(column)
        try{
            return df.withColumn("year", date_format(col(column), "yyyy"))
                        .withColumn("month", date_format(col(column), "MM"))
                        .withColumn("day", date_format(col(column), "dd"))
            
        }
        catch {
            case x: Exception =>
                {
                    throw new FileFormatException("error creating the columns")
                }    
        }
    }


    def transformColumn(df: DataFrame, column: String, function: String => String): DataFrame = {
        //this.validateTable(df)
        this.validateColumn(column)
        try {
            import org.apache.spark.sql.functions.udf
            val udfFunction = udf(function)
            return df.withColumn(column, udfFunction(col(column)))
        }
        catch{
            case x: Exception =>
                {
                    throw new FileFormatException("error transforming the columns")
                }
        }
    }


    def transformColumns(df: DataFrame, columns: Seq[String], function: String => String): DataFrame = {
        //this.validateTable(df)
        this.validateColumns(columns)
        @transient lazy val fc = function
        try {
            import org.apache.spark.sql.functions.udf
            val udfFunction = udf(fc)
            var tmpDf = df
            for (column <- columns) {
                    tmpDf = tmpDf.withColumn(column, when(col(column).isNotNull,udfFunction(col(column))).otherwise(lit(null)))
            }
            return tmpDf
        }
        catch{
            case x: Exception =>
                {
                    throw new FileFormatException("error transforming the columns")
                }
        }
    }

    def selectColumns(df: DataFrame, columns: Seq[String]): DataFrame = {
        //this.validateTable(df)
        this.validateColumns(columns)
        try{
            
            return df.select(columns.map(x => col(x)).toSeq: _*)
        } 
        catch{
            case x: Exception =>
                {
                    throw new FileFormatException("error selecting the columns")
                }
        }
    }

    def findFirstValidPath(paths: Seq[String]): String = {
        if (paths.isEmpty){
             throw new FileFormatException("Paths cannot be empty")
        }
        else {
            for (path <- paths){
                if (this.existFiles(path) == true){
                    return path
                }
            }
            return ""
        }
    }

    def getSchema(table: DataFrame): Seq[String] = {
        this.validateTable(table)
        return table.schema.map(x => x.name).toSeq

    }

    def close(): Unit = {
        if (this.session == null){
            throw new FileFormatException(sessionErrorMsg)
        }
        else{
            this.session.close()
        }
    }
}