package com.turingfactory.adapters
import com.turingfactory.ports.Encryptor
import com.turingfactory.exceptions.EncryptorException
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import java.security.GeneralSecurityException
import java.security.InvalidAlgorithmParameterException
import java.security.InvalidKeyException
import java.security.spec.AlgorithmParameterSpec
import java.security.NoSuchAlgorithmException
import javax.crypto.NoSuchPaddingException
import java.util.Base64
import javax.crypto.BadPaddingException
import javax.crypto.Cipher
import javax.crypto.IllegalBlockSizeException
import javax.crypto.SecretKey
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec
import org.apache.commons.lang3.StringUtils
import java.math.BigInteger
import org.apache.commons.codec.binary.Hex

class AesEncryptor extends Encryptor with Serializable {

    private val transformation: String = "AES/CBC/PKCS5Padding"
    private val algorithm: String = "AES"
    private var secretKey: SecretKeySpec = null
    private var encryptor: Cipher = null
    private var decryptor: Cipher = null
    
    def this(key: String, iv: String){
        this()
        this.validateKey(key)
        this.validateIv(iv)
        this.initEncryptor(this.transformation, key, iv, this.algorithm)
        this.initDecryptor(this.transformation, key, iv, this.algorithm)
    }

    private def validateTransformation(transformation: String): Boolean = {
        transformation match {
            case null => throw new EncryptorException("transformation cannot be null")
            case transformation if transformation.isEmpty => throw new EncryptorException("transformation cannot be empty")
            case _ => return true
        }     
    }

    private def getCipherInstance(transformation: String): Cipher = {
        this.validateTransformation(transformation)
        try {
            return Cipher.getInstance(transformation)
        } catch{
            case e: NoSuchAlgorithmException =>
            {
                throw new EncryptorException("invalid transformation format")
            }
            case e: NoSuchPaddingException =>
            {
                throw new EncryptorException("padding scheme not available")
            }
        }
    }

    private def validateKey(key: String): Boolean = {
        key match {
            case null => throw new EncryptorException("key cannot be null")
            case key if key.isEmpty => throw new EncryptorException("key cannot be empty")
            case _ => return true
        }     
    }

    private def validateIv(iv: String): Boolean = {
        iv match {
            case null => throw new EncryptorException("iv cannot be null")
            case iv if iv.isEmpty => throw new EncryptorException("iv cannot be empty")
            case _ => return true
        }     
    }

    private def validateAlgorithm(algorithm: String): Boolean = {
        algorithm match {
            case null => throw new EncryptorException("algorithm cannot be null")
            case algorithm if algorithm.isEmpty => throw new EncryptorException("algorithm cannot be empty")
            case _ => return true
        }     
    }

    private def validateText(text: String): Boolean = {
        text match {
            case null => throw new EncryptorException("text cannot be null")
            case text if text.isEmpty => throw new EncryptorException("text cannot be empty")
            case _ => return true
        }     
    }

    private def validateEncryptedText(encryptedText: String): Boolean = {
        encryptedText match {
            case null => throw new EncryptorException("encryptedText cannot be null")
            case encryptedText if encryptedText.isEmpty => throw new EncryptorException("encryptedText cannot be empty")
            case _ => return true
        }     
    }

    private def initEncryptor(transformation: String, key: String, iv: String, algorithm: String): Unit ={
        this.encryptor = getCipherInstance(transformation)
        import java.util.Base64
        val secretKey = new SecretKeySpec(Base64.getDecoder.decode(key.getBytes(StandardCharsets.UTF_8)), algorithm)
        val algorithmParameterSpec = new IvParameterSpec(Base64.getDecoder.decode(iv.getBytes(StandardCharsets.UTF_8)))
        try {
            this.encryptor.init(Cipher.ENCRYPT_MODE, secretKey, algorithmParameterSpec)
        }
        catch{
            case x: InvalidKeyException =>
            {
                throw new EncryptorException("invalid secretKey")
            }
            case x: InvalidAlgorithmParameterException =>
            {
                throw new EncryptorException("invalid algorithmParameterSpec")
            }
        }
    }

    private def initDecryptor(transformation: String, key: String, iv: String, algorithm: String): Unit ={
        this.decryptor = getCipherInstance(transformation)
        this.validateAlgorithm(algorithm)
        import java.util.Base64
        val secretKey = new SecretKeySpec(Base64.getDecoder.decode(key.getBytes(StandardCharsets.UTF_8)), algorithm)
        val algorithmParameterSpec = new IvParameterSpec(Base64.getDecoder.decode(iv.getBytes(StandardCharsets.UTF_8)))
        try {
            this.decryptor.init(Cipher.DECRYPT_MODE, secretKey, algorithmParameterSpec)
        }
        catch{
            case x: InvalidKeyException =>
            {
                throw new EncryptorException("invalid secretKey")
            }
            case x: InvalidAlgorithmParameterException =>
            {
                throw new EncryptorException("invalid algorithmParameterSpec")
            }
        }
    }

    def encrypt(text: String): String = {
        validateText(text)
        return Hex.encodeHexString(this.encryptor.doFinal(text.getBytes(StandardCharsets.UTF_8)))
    }

    def encryptToB64(text: String): String = {
        validateText(text)
        import java.util.Base64
        return Base64.getEncoder.encodeToString(this.encryptor.doFinal(text.getBytes(StandardCharsets.UTF_8)))
    }

    def decrypt(encryptedText: String): String = {
        validateEncryptedText(encryptedText)
        return new String(this.decryptor.doFinal(Hex.decodeHex(encryptedText.toCharArray())))
    }
}
