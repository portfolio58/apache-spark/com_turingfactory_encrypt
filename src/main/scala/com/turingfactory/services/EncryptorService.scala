package com.turingfactory.services
import com.turingfactory.adapters.{AesEncryptor, FileFormatOperator}
import com.turingfactory.models.{Request, Response, ResponseFailure, ResponseSuccess}
import com.turingfactory.ports.{Encryptor}
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types.StringType

class EncryptorService() extends Serializable {
    var fileOperator: FileFormatOperator = _
    var encryptor: Encryptor = _
    def this(fileOperator: FileFormatOperator, encryptor: Encryptor){
        this()
        if (fileOperator == null) {
            throw new IllegalArgumentException("file operator cannot be null")
        }
        if (encryptor == null){
            throw new IllegalArgumentException("encryptor cannot be null")
        }
        this.fileOperator = fileOperator
        @transient lazy val encryptortmp = encryptor
        this.encryptor = encryptortmp
        
    }

    def invoke(request: Request): Response = {

        try {
            request.isValid
            val data = fileOperator.getTable(request.inputPath, request.inputFileFormat)
            val conversionMap = request.columnsToEncrypt.map(a => a -> StringType).toMap
            val correctedTable = fileOperator.castColumns(data, conversionMap.map(x=> (col(x._1), x._2)))
            
            val encryptedCorrelators = fileOperator.transformColumns(correctedTable, request.columnsToEncrypt, encryptor.encrypt(_))

            fileOperator.saveTable(request.outputPath, encryptedCorrelators)
            fileOperator.close()
            val response = new ResponseSuccess("EncryptorService","runned successfully")
            return response
        }
        catch {
                case e:Exception => {
                    val response = new ResponseFailure(e.getClass.toString, e.getMessage.toString)

                return response
            }
        }
    }
}
