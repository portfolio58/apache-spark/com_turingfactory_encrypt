package com.turingfactory.ports
trait Encryptor {
    def encrypt(text: String): String
    def encryptToB64(text: String): String
    def decrypt(encryptedText: String): String
}