package com.turingfactory

import com.turingfactory.adapters.{FileFormatOperator, Base64Encoder, SparkSessionFactory, SpringEncryptor}
import com.turingfactory.exceptions.ControllerException
import com.turingfactory.models.{Request, Response}
import com.turingfactory.ports.{Encoder, Encryptor}
import com.turingfactory.services.EncryptorService
import org.apache.spark.sql.SparkSession
import org.json4s._
import org.json4s.native.Serialization
import org.json4s.native.Serialization.read
import org.springframework.security.crypto.encrypt.{Encryptors, TextEncryptor}
object App extends Serializable{

    // private def validateResponse(response: Response): Boolean = {
    //     if(!response.isValid){
    //         throw new ControllerException(response.toString) 
    //         return false
    //     }else{
    //         System.exit(0)
    //     }
    // }

    private def makeOperator(session: SparkSession): FileFormatOperator = {
        try{

            val operator: FileFormatOperator = new FileFormatOperator(session)
            return operator
        }
        catch{
            case e:Exception => {
                throw new ControllerException(e.getClass.toString.concat(":").concat(e.getMessage.toString))
            }   
        }
    }

     private def makeRequest(encodedInput: String, encoder: Encoder): Request = {
        try{
            val jsonInput = encoder.decode(encodedInput)
            implicit val formats = Serialization.formats(NoTypeHints)
            return read[Request](jsonInput)
        }
        catch{
            case e:Exception => {
                throw new ControllerException(e.getClass.toString.concat(":").concat(e.getMessage.toString))
            }   
        }
    }


    def main(args: Array[String]): Unit = {

        val encodedInput = args(0)
        try{
            val sessionFactory = new SparkSessionFactory()
            val session = sessionFactory.makeCluster()
            val encoder: Encoder = new Base64Encoder
            val request: Request = this.makeRequest(encodedInput, encoder)
            val operator: FileFormatOperator = this.makeOperator(session)
            @transient lazy val encryptor: Encryptor = new SpringEncryptor(request.password, request.salt)
            val service: EncryptorService = new EncryptorService(operator, encryptor)
            val response: Response = service.invoke(request)
            if (response.isValid){
                session.stop()
                System.exit(0)
            }else{
                throw new ControllerException(response.toString) 
            }

        }
        catch{
            case e:Exception => {
                throw new ControllerException(e.getMessage)
            }
        }
    }
}
