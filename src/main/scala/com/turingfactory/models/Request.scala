package com.turingfactory.models
import scala.collection.mutable.Map
import com.turingfactory.exceptions.RequestException

class Request() extends Serializable {

    var errorList: Map[String, String] = Map.empty[String, String]
    val errorMessage = "cannot be null or empty"

    var inputPath: String = _
    var outputPath: String = _
    var inputFileFormat: String = _
    var outputFileFormat: String = _
    var password: String = _
    var salt: String = _
    var encryptorType: String = _
    var columnsToEncrypt: Seq[String] = _

    def addError(field: String, message: String) = {
        errorList += (field -> message)
    }

    def this(inputPath: String, outputPath: String, inputFileFormat: String, outputFileFormat: String, password: String, salt: String, encryptorType: String, columnsToEncrypt: String){
        this()

        if (inputPath == null || inputPath.isEmpty) {
            addError("inputPath", errorMessage)
        } else {
            this.inputPath = inputPath
        }

        if (outputPath == null || outputPath.isEmpty) {
            addError("outputPath", errorMessage)
        } else {
            this.outputPath = outputPath
        }

        if (inputFileFormat == null || inputFileFormat.isEmpty) {
            addError("inputFileFormat", errorMessage)
        } else {
            this.inputFileFormat = inputFileFormat
        }

        if (outputFileFormat == null || outputFileFormat.isEmpty) {
            addError("outputFileFormat", errorMessage)
        } else {
            this.outputFileFormat = outputFileFormat
        }

        if (password == null || password.isEmpty) {
            addError("password", errorMessage)
        } else {
            this.password = password
        }

        if (salt == null || salt.isEmpty) {
            addError("salt", errorMessage)
        } else {
            this.salt = salt
        }

        if (encryptorType == null || encryptorType.isEmpty) {
            addError("encryptorType", errorMessage)
        } else {
            this.encryptorType = encryptorType
        }

        if (columnsToEncrypt == null || columnsToEncrypt.isEmpty) {
            addError("columnsToEncrypt", errorMessage)
        } else {
            this.columnsToEncrypt = columnsToEncrypt.split(",").toSeq
        }



    }

    def isValid(): Boolean = {
        if (!this.errorList.isEmpty){
            throw new RequestException("this arguments cannot be empty or null:".concat(this.errorList.toString))
            return false
        }
        else{
            return this.errorList.isEmpty
        }
    }

}